﻿using System.Reflection;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
namespace Harmony
{
    public class BlockBuffAOE : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out(" Loading Patch: " + GetType());

            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
    [HarmonyPatch(typeof(TileEntity))]
    [HarmonyPatch("Instantiate")]
    public class TileEntity_Instantiate
    {
        public static bool
        Prefix(ref TileEntity __result, TileEntityType type, Chunk _chunk)
        {
            if (type == (TileEntityType)244)
            {
                __result = (TileEntity)new TileEntityAlwaysActive(_chunk);
                return false;
            }
            return true;
        }
    }
}