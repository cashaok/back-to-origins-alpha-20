Key,russian

gunMGT3BrowningM1919,"Пулемет .30 Калибра"
gunMGT3BrowningM1919Desc,"The M1919 Browning is a .30 caliber medium machine gun that was widely used during the 20th century, especially during World War II, the Korean War, and the Vietnam War. The M1919 saw service as a light infantry, coaxial, mounted, aircraft, and anti-aircraft machine gun by the U.S. and many other countries.",,
gunMGT3BrowningM1919Schematic,"Схема Пулемет .30 Калибра"
gunM1919A4Schematic,"Схема Пулемет .30 Калибра"

